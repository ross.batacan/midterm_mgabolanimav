CSCI 40 - C
Batacan, Ross Yvailo G. - 210756
Lee, Mavrick Jordan L. - 213374
Suplico, Enrico Lucio A. - 215635
Final Project: Widget v2

Member's App Assignments:
Dashboard - Mavrick Jordan Lee
Announcement Board - Ross Yvailo Batacan
Forum - Enrico Lucio Suplico

Date of Submission: May 15, 2023

This lab was truthfully completed by our group with members Ross Yvailo Batacan, Mavrick Jordan Lee, and Enrico Lucio Suplico.

References:


(sgd) Ross Yvailo G. Batacan, 05/14/2023
(sgd) Mavrick Jordan L. Lee, 05/14/2023
(sgd) Enrico Lucio Suplico, 05/14/2023