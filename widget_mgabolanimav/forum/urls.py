from django.urls import path
from.views import Forum, ForumDetailViews, ForumCreateView, ForumEditView

urlpatterns = [
   path('', Forum, name="forum"),
   path('<int:pk>/details/', ForumDetailViews.as_view(), name="forumpost-details"),
   path('add/', ForumCreateView.as_view(), name="forumpost-add"),
   path('<int:pk>/edit/', ForumEditView.as_view(), name="forumpost-edit"),
]

app_name = 'forum'