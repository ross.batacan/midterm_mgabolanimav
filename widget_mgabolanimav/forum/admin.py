from django.contrib import admin

# Register your models here.

from .models import ForumPost, Reply

class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost

    list_display = ('title', 'pub_datetime',)
    search_fields = ('title',)
    list_filter = ('title', 'pub_datetime',)


class ReplyAdmin(admin.ModelAdmin):
    model = Reply

    list_display = ('pub_datetime',)
    list_filter = ('pub_datetime',)


admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)


