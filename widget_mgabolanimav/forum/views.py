from .models import ForumPost, Reply
from django.http import HttpResponse
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render

# Create your views here.

def Forum(request):
    forums = ForumPost.objects.order_by("-pub_datetime")
    context = {
        "forums": forums,
    }

    return render(request, 'forum/forum.html', context)


class ForumDetailViews(DetailView):
    model = ForumPost
    template_name = 'forum/forumpost-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['replies'] = Reply.objects.filter(forum_post=self.object)
        return context
    
class ForumCreateView(CreateView):
    model = ForumPost
    fields = 'title', 'body', 'author'
    template_name = 'forum/forumpost-add.html'

class ForumEditView(UpdateView):
    model = ForumPost
    fields = 'title', 'body', 'author'
    template_name = 'forum/forumpost-edit.html'


