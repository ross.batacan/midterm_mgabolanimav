from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse
from django.utils import timezone

# Create your models here.

class ForumPost(models.Model):
    title = models.CharField(max_length = 50, unique = True, default = "", null = True,)
    body = models.TextField(default = "", null = True,)
    author = models.ForeignKey (WidgetUser, on_delete = models.CASCADE, default = "", null = True,)
    pub_datetime = models.DateTimeField(null = True,)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.pk:
            self.pub_datetime = timezone.now()
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('forum:forumpost-details', kwargs={'pk': self.pk})



class Reply(models.Model):
    body = models.TextField(default = "", null = True,)
    author = models.ForeignKey(WidgetUser, on_delete = models.CASCADE, default = "", null = True,)
    pub_datetime = models.DateTimeField(null = True,)
    forum_post = models.ForeignKey(ForumPost, on_delete = models.CASCADE, null = True,)






