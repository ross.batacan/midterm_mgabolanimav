from django.urls import path
from .views import Announcements, AnnouncementsDetailsView, AnnouncementsCreateView, AnnouncementsEditView

urlpatterns = [
   path('', Announcements, name="announcements"),
   path('<int:pk>/details/', AnnouncementsDetailsView.as_view(), name="announcement-details"),
   path('add/', AnnouncementsCreateView.as_view(), name="announcement-add"),
   path('<int:pk>/edit/', AnnouncementsEditView.as_view(), name="announcement-edit"),
]

app_name = 'announcements'