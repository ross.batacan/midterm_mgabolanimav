from django.contrib import admin
from .models import Announcement, Reaction


# Register your models here.

class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement
    list_display = ('announcement_title', 'announcement_body', 'announcement_pub_datetime')
    search_fields = ('announcement_title',)
    list_filter = ('announcement_title',)


class ReactionAdmin(admin.ModelAdmin):
    model = Reaction
    list_display = ('reaction_name', 'reaction_tally', 'reaction_announcement',)
    search_fields = ('reaction_name',)
    list_filter = ('reaction_name',)


admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Reaction, ReactionAdmin)
