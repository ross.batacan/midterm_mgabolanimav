from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import Announcement, Reaction


# Create your views here.
def Announcements(request):
    announcements = Announcement.objects.order_by("-announcement_pub_datetime")
    context = {
        "announcements": announcements,
    }

    return render(request, 'announcements/announcements.html', context)


class AnnouncementsDetailsView(DetailView):
    model = Announcement
    template_name = 'announcements/announcement-details.html'\


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['reactions'] = Reaction.objects.filter(reaction_announcement=self.object)
        return context


class AnnouncementsCreateView(CreateView):
    model = Announcement
    fields = 'announcement_title', 'announcement_body', 'announcement_author'
    template_name = 'announcements/announcement-add.html'


class AnnouncementsEditView(UpdateView):
    model = Announcement
    fields = 'announcement_title', 'announcement_body', 'announcement_author'
    template_name = 'announcements/announcement-edit.html'
