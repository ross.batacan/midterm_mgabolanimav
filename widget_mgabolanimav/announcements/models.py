from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse
from django.utils import timezone


# Create your models here.

# please insert foreign key from dashboard application on announcement_author
class Announcement(models.Model):
    announcement_title = models.CharField(max_length=50, unique=True, null=True, verbose_name="Title")
    announcement_body = models.TextField(max_length=250, unique=True, null=True, verbose_name="Body")
    announcement_author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE, null=True, verbose_name="Author")
    announcement_pub_datetime = models.DateTimeField(null=True, )

    def __str__(self):
        return self.announcement_title

    def save(self, *args, **kwargs):
        if not self.pk:
            self.announcement_pub_datetime = timezone.now()
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('announcements:announcement-details', kwargs={'pk': self.pk})


reaction_choices = (
    ('Like', 'Like'),
    ('Love', 'Love'),
    ('Angry', 'Angry'),
)


class Reaction(models.Model):
    reaction_name = models.CharField(max_length=50, blank=True, choices=reaction_choices, null=True, )
    reaction_tally = models.IntegerField(null=True, default=0, )
    reaction_announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE, null=True, )

    def __str__(self):
        return self.reaction_name

