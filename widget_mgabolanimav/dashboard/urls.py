from django.urls import path

from . import views
from .views import (
    WidgetUserDetailsView, WidgetUserCreateView, WidgetUserUpdateView
)

urlpatterns = [
    path('dashboard/', views.dashboard, name = "dashboard"),
    path('widgetusers/<int:pk>/details/', WidgetUserDetailsView.as_view(), name = "widgetuser-detail"),
    path('widgetusers/add/', WidgetUserCreateView.as_view(), name = "widgetuser-add"),
    path('widgetusers/<int:pk>/edit/', WidgetUserUpdateView.as_view(), name = "widgetuser-edit"),
]

app_name = 'dashboard'