from django.contrib import admin

from .models import Department, WidgetUser

# Register your models here.

class DepartmentAdmin(admin.ModelAdmin):
    model = Department
    list_display = ('dept_name', 'home_unit')
    search_fields = ('dept_name', 'home_unit')
    list_filter = ('dept_name', 'home_unit')

class WidgetUserAdmin(admin.ModelAdmin):
    model = WidgetUser
    list_display = ('first_name', 'middle_name', 'last_name', 'user_department')
    search_fields = ('first_name', 'middle_name', 'last_name', 'user_department')
    list_filter = ('first_name', 'middle_name', 'last_name', 'user_department')

admin.site.register(Department, DepartmentAdmin)
admin.site.register(WidgetUser, WidgetUserAdmin)
