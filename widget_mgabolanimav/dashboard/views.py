from .models import WidgetUser

from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

# Create your views here.
def dashboard(request): 
    widget_users = WidgetUser.objects.all()
    context = {"widget_users": widget_users}
    return render(request, 'dashboard/dashboard.html', context)

class WidgetUserDetailsView(DetailView):
    model = WidgetUser
    template_name = 'dashboard/widgetuser-details.html'

class WidgetUserCreateView(CreateView):
    model = WidgetUser
    fields = '__all__'
    template_name = 'dashboard/widgetuser-add.html'

class WidgetUserUpdateView(UpdateView):
    model = WidgetUser
    fields = '__all__'
    template_name = 'dashboard/widgetuser-edit.html'
