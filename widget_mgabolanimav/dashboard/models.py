from django.db import models
from django.urls import reverse

# Create your models here.

class Department(models.Model):
    dept_name = models.CharField(max_length=50, unique=True, null=True)
    home_unit = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.dept_name

class WidgetUser(models.Model):
    first_name = models.CharField(max_length=30, unique=True, null=True)
    middle_name = models.CharField(max_length=20, unique=True, null=True)
    last_name = models.CharField(max_length=20, unique=True, null=True)
    user_department = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return self.first_name
    
    def get_absolute_url(self):
        return reverse('dashboard:widgetuser-detail', kwargs={'pk': self.pk})
